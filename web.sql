-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-02-2019 a las 19:25:02
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `web`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcolaboradores`
--

DROP TABLE IF EXISTS `tblcolaboradores`;
CREATE TABLE IF NOT EXISTS `tblcolaboradores` (
  `id_colaborador` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(10) NOT NULL COMMENT '12345678-K',
  `nombres` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `apellido_materno` varchar(255) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `sexo` varchar(1) NOT NULL DEFAULT 'M',
  `fecha_nacimiento` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `email-personal` varchar(255) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `updated-on` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_colaborador`),
  UNIQUE KEY `rut` (`rut`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcolaboradores`
--

INSERT INTO `tblcolaboradores` (`id_colaborador`, `rut`, `nombres`, `apellido`, `apellido_materno`, `cargo`, `descripcion`, `sexo`, `fecha_nacimiento`, `email`, `email-personal`, `activo`, `updated-on`) VALUES
(1, '14049653-7', 'Nicolas', 'Morell', 'Moraga', '', '', 'H', '1980-08-01', 'nmorell@vgroup.cl', 'nmorell@gmail.com', 0, '2019-02-28 19:21:03'),
(2, '13232843-9', 'Macarena', 'Carmona', 'Bravo', '', '', 'M', '1977-05-05', 'mcarmona@vgroup.cl', 'macacarmona@hotmail.com', 1, '2019-02-28 18:47:35'),
(3, '16470741-5', 'Jorge', 'Velarde', 'Rodriguez', '', '', 'H', '1985-08-25', 'jvelarde@vgroup.cl', 'jorge.velarde.r@gmail.com', 0, '2018-03-07 20:06:12'),
(4, '9797576-0', 'Daniela', 'Krauss', 'Bonilla', '', '', 'M', '1971-07-30', 'dkrauss@vgroup.cl', 'dkrauss3007@gmail.com', 1, '2019-02-28 18:43:47'),
(5, '16369681-9', 'Belén', 'Oliveri', 'Gajardo', '', '', 'M', '1987-08-21', 'boliveri@vgroup.cl', 'b.oliverig@gmail.com', 1, '2017-04-26 14:52:52'),
(6, '17863365-1', 'Javiera', 'Concha', 'Vidal', '', '', 'M', '1991-03-26', 'jconcha@vgroup.cl', 'jlconcha@uc.cl', 0, '2017-07-27 13:30:40'),
(7, '15555966-7', 'Arlene', 'Quezada', 'Navarrete', '', '', 'M', '1982-12-30', 'aquezada@vgroup.cl', 'arlenneq@gmail.com', 1, '2017-04-10 16:02:55'),
(8, '16014636-2', 'Rosalia', 'Fernandez', 'Galnares', '', '', 'M', '1984-12-26', 'rfernandez@vgroup.cl', 'fernandez.g.rosalia@gmail.com', 0, '2016-12-22 12:51:15'),
(9, '15897840-7', 'Tiare', 'Donoso', 'Mena', '', '', 'M', '1984-05-14', 'tdonoso@vgroup.cl', 'pstiaredonoso@gmail.com', 0, '2018-01-19 15:34:41'),
(10, '16264603-6', 'Rolando', 'Vidaura', 'Vargas', '', '', 'H', '1985-09-20', 'rvidaura@vgroup.cl', '', 0, '2018-03-07 20:06:16'),
(11, '15448606-2', 'Manuel', 'Estrada', 'Flores', '', '', 'H', '1983-06-27', 'mestrada@vgroup.cl', '', 0, '2018-03-07 20:06:17'),
(13, '18385862-9', 'Cristian', 'Canales', 'Ojeda', '', '', 'H', '1993-07-31', 'ccanales@vgroup.cl', 'cristian.canales06@hotmail.com', 1, '2018-03-15 15:01:02'),
(14, '9874030-9', 'Magdalena', 'Auger', 'Court', '', '', 'M', '1970-07-01', 'mauger@vgroup.cl', '', 1, '2016-01-01 03:00:00'),
(15, '15149104-9', 'Jorge', 'Gonzalez', 'Reyes', '', '', 'H', '1980-03-05', 'jgonzalez@vgroup.cl', '', 0, '2018-03-07 20:06:20'),
(18, '17786690-3', 'Javiera', 'Guiñez', 'Zuñiga', '', '', 'M', '1991-10-05', 'jguinez@vgroup.cl', 'javieraguinez221@gmail.com', 1, '2017-07-05 13:40:41'),
(19, '13026999-0', 'Isabel Margarita', 'Avalos', 'Solar', '', '', 'M', '1976-05-13', 'iavalos@vgroup.cl', '', 0, '2017-04-04 15:41:50'),
(20, '16517907-2', 'Valeria Andrea', 'Peñaloza', 'Martinez', '', '', 'M', '1986-11-05', 'vpenaloza@vgroup.cl', '', 0, '2016-09-21 13:03:47'),
(21, '13473617-8', 'Mariana Ivonne', 'Machuca', 'Loyer', '', '', 'M', '1978-09-07', 'mmachuca@vgroup.cl', '', 0, '2016-12-20 13:49:53'),
(22, '17024395-1', 'Wolfgang', 'Henseleit', 'Concha', '', '', 'H', '1988-09-22', 'whenseleit@vgroup.cl', '', 0, '2018-03-07 20:06:24'),
(23, '13242125-0', 'Catalina', 'Fernandez', 'de la Maza', '', '', 'M', '1977-12-20', 'cfernandez@vgroup.cl', '', 0, '2017-04-04 15:41:57'),
(24, '25487550-3', 'Nazareth', 'Montilla', 'Civilla', '', '', 'M', '1985-01-17', 'nmontilla@vgroup.cl', 'nazarethmontillacivila@gmail.com', 1, '2017-11-29 18:09:59'),
(25, '15601821-K', 'Mariano', 'Lopez', 'Navarrete', '', '', 'H', '1983-05-16', 'mlopez@vgroup.cl', 'mariano.lopez.n@gmail.com', 1, '2018-03-07 20:06:46'),
(26, '15675734-9', 'Carolina', 'Castillo', 'Espinosa', '', '', 'M', '1983-12-04', 'ccastillo@vgroup.cl', 'ccastilloes@gmail.com', 0, '2019-01-21 14:12:17'),
(27, '14364990-3', 'Natalia', 'Molina', 'Rojas', '', '', 'M', '1981-02-19', 'nmolina@vgroup.cl', 'nataliamolina27@gmail.com', 1, '2017-02-13 18:46:59'),
(28, '17782003-2', 'Bastián', 'León ', 'Herrera', '', '', 'M', '1991-04-21', 'bleon@vgroup.cl', 'bastianleon@icloud.com', 0, '2017-04-10 03:13:16'),
(29, '17407843-2', 'Arlette Eficiencia', 'Gassibe', 'Booth', '', '', 'M', '1990-06-30', 'agassibe@vgroup.cl', 'arlettegassibe@gmail.com', 1, '2018-03-27 14:54:40'),
(30, '16281392-7', 'Paulina', 'Becerra', 'Salinas', '', '', 'M', '1986-07-29', 'pbecerra@vgroup.cl', 'becerra.pau@gmail.com', 0, '2017-07-04 20:54:17'),
(31, '17840104-1', 'Yesenia', 'Leiva', 'Arancibia', '', '', 'M', '1991-06-02', 'yleiva@vgroup.cl', 'yes.leiva.ara@gmail.com', 0, '2018-11-05 14:10:08'),
(32, '26100126-8', 'David', 'Sotillo', 'Aguiar', '', '', 'H', '1995-04-22', 'dsotillo@vgroup.cl', 'david.sotillo22@gmail.com', 1, '2018-03-07 20:06:30'),
(33, '17708532-4', 'Victor Eduardo', 'Curilao', 'Orellana', '', '', 'H', '1991-08-22', 'vcurilao@vgroup.cl', 'victor.edu22@gmail.com', 1, '2018-03-15 15:01:07'),
(34, '25981860-5', 'Aldith', 'Avila', 'Torrellas', '', '', 'M', '1986-08-09', 'aavila@vgroup.cl', 'adilthavila@gmail.com', 1, '2018-02-20 14:45:27'),
(35, '26119012-5', 'Andrea Carolina', 'Cova', 'López', '', '', 'M', '1993-06-26', 'acova@vgroup.cl', 'andreacovadg@gmail.com', 0, '2018-11-05 13:33:42'),
(36, '17850628-5', 'Josias Israel', 'Muñoz', 'Bettancourt', '', '', 'M', '1991-06-06', 'jmunoz@vgroup.cl', 'josias.munoz@gmail.com', 0, '2018-11-05 14:10:04'),
(37, '16748477-8', 'Lindsay Diana', 'Inzunza', 'Peñaloza', '', '', 'F', '1988-02-17', 'linzunza@vgroup.cl', 'lindsay.inzunza@gmail.com', 0, '2018-04-09 20:59:26'),
(38, '17872510-6', 'Nataly Isabel', 'Gaete', 'Rodriguez', '', '', 'F', '1991-12-06', 'ngaete@vgroup.cl', 'natalyisabelgaete@gmail.com', 0, '2018-11-05 14:09:58'),
(39, '14144407-7', 'Vanessa Carolina', 'Vergara', 'Villagrán', '', '', 'F', '1981-01-29', 'vvergara@vgroup.cl', 'v.vergara.v@gmail.com', 0, '2019-01-21 14:12:43'),
(40, '13049071-9', 'Alejandra María', 'Carrasco', 'Soto', '', '', 'F', '1981-01-29', 'acarrasco@vgroup.cl', 'x', 1, '2018-07-25 13:51:26'),
(41, '15314986-0', 'Macarena', 'Rivera', 'Cruz', '', '', 'F', '1983-07-25', 'mrivera@vgroup.cl', 'macarena.r.cruz@gmail.com', 0, '2018-12-19 17:54:10'),
(42, '15477302-9', 'David Abraham', 'Ormeño', 'Vargas', '', '', 'M', '1983-03-04', 'dormeno@vgroup.cl', 'ov.david@gmail.com', 0, '2018-11-05 13:33:11'),
(43, '18570103-4', 'Daniela Paz', 'Pineda', 'Jacob', '', '', 'F', '1993-08-26', 'dpineda@vgroup.cl', 'danielapinedajacob@gmail.com', 0, '2018-11-05 13:33:18'),
(47, '16116907-2', 'Daniela Esther', 'Soto', 'Álvarez', '', '', 'F', '1985-08-07', 'dsoto@vgroup.cl', 'danielae.sotoa@gmail.com', 1, '2018-08-30 13:07:51'),
(48, '1221509', 'Anyely Yesenia', 'Machuca', 'Arrieta', '', '', 'F', '1995-08-10', 'amachuca@vgroup.cl', 'anyelymachuca@gmail.com', 1, '2018-11-05 14:11:14'),
(49, '123', 'Juan Jose', 'Cifuentes', '-', '', '', 'M', '1980-08-10', 'jcifuentes@vgroup.cl', '-', 1, '2019-01-21 14:11:14'),
(51, '1234', 'Rodrigo', 'Valenzuela', '-', '', '', 'M', '1980-08-10', 'rvalenzuela@vgroup.cl', '-', 1, '2019-01-21 14:11:14'),
(52, '12345', 'Marilyn', 'Vargas', '-', '', '', 'F', '1980-08-10', 'mvargas@vgroup.cl', '-', 1, '2019-01-21 14:11:14'),
(53, '123454', 'Rene', 'Aguilera', '-', '', '', 'M', '1980-08-10', 'raguilera@vgroup.cl', '-', 1, '2019-01-21 14:11:14'),
(55, '12345454', 'Anyi', 'Machuca', '-', '', '', 'F', '1980-08-10', 'amachuca@vgroup.cl', '-', 1, '2019-01-21 14:11:14'),
(57, '18050444-3', 'Diego', 'Vega', 'Orellana', 'Desarrollador', '', 'M', '1991-12-01', 'dvega@vgroup.cl', 'diegoavo@gmail.com', 1, '2019-02-28 14:49:37'),
(58, '16115160-2', 'Julia', 'Manzzo', '-', '', '', 'F', '1985-04-23', 'jmanzzo@vgroup.cl', 'julitamanzzo@gmail.com', 1, '2019-02-14 20:56:00'),
(59, '16510833-7', 'Carolina', 'Cuevas', '-', '', '', 'F', '1986-08-08', 'ccuevas@vgroup.cl', 'karocuevas@gmail.com', 1, '2019-02-14 20:56:09'),
(60, '', 'prueba', 'PRUEBA', '', 'prueba', 'sad', 'M', '0000-00-00', 'juan@perez.cl', '', 0, '2019-02-28 18:46:00'),
(62, '123456789', 'prueba', 'PRUEBA', '', 'ctm', 'wedsf', 'M', '0000-00-00', 'juan@perez.cl', '', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcontacto`
--

DROP TABLE IF EXISTS `tblcontacto`;
CREATE TABLE IF NOT EXISTS `tblcontacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mensaje` varchar(2000) NOT NULL,
  `fechaCreacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarioslogin`
--

DROP TABLE IF EXISTS `tblusuarioslogin`;
CREATE TABLE IF NOT EXISTS `tblusuarioslogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblusuarioslogin`
--

INSERT INTO `tblusuarioslogin` (`id`, `nombre`, `apellido`, `email`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'diego', 'vega', 'dvega@vgroup.cl', '$2y$10$x/mqmHChQ7KJHD06sDjjUuSb.nndCNuzrTWpuh.oDg6M/bj5nB5EG', '2019-02-25 15:17:19', '2019-02-28 21:44:26', 'CF7418XGxMHBeiSjr5gEIqxmsb8a8hm4DkA7acqrMsuL9kh2MZQ7XaJ4Tko5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cargo` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `url_foto` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `cargo`, `descripcion`, `email`, `url_foto`, `estado`) VALUES
(1, 'd', 'd', 'afghfg', 'diego@gamkbjhg.com', 'saodaksdokdas.cl', 1),
(2, 'francisco', 'aseador', 'es una persona muy buena', 'franci@gmail.com', 'www.tufoto.cl', 0),
(3, 'jorge', 'ingeniero', 'es una mala persona XD', 'jorgelcurioso@gmail.com', 'www.aodfjaosdjf.cl', 0),
(4, 'pedro', 'idasjd', 'asidjiasdj', 'sidjisdjfi', 'asidjiaj', 0),
(21, 'pedro', 'saodk', 'asdp', 'wqdko@sdm.cl', 'asdd', 0),
(20, 'hola', 'fd', 'gdfg', 'gfgh@sadfsd.cl', 'dfg', 0),
(19, 'criss', 'dev', 'sdopfis', 'asd@asud.cl', 'asdas', 0),
(18, 'es', 'asd', 'as', 'asd@diasj.cl', 'sad', 0),
(17, 'hoa', 'asd', 'asdasd', 'asd@gmadsf.cl', 'asda', 0),
(16, 'fgdf', 'fdg', 'dfgdsfg', 'fdg@sadjf.cl', 'sijdias', 0),
(15, 'juliotriviño', 'ventas', 'es una persona muy simpatica', 'julio@gmail.com', 'oasdkoask', 0),
(22, 'tulio', 'animador', 'sdpclsd', 'asd@diasj.cl', 'dsfsdf', 0),
(23, 'prueba200', 'hui', 'wqeqwe', 'diego@gafghs.com', 'asd', 0),
(24, 'prueba201', 'fsdf', 'sdfksdfm', 'das@gma.cl', 'asod', 0),
(25, 'ahora si me sale', 'ctm', 'asdas', 'asd@diasj.cl', 'sad', 0),
(26, 'sdf', 'sdf', 'sdfdf', 'diegasdas@as.com', 'sdfsdf', 1),
(27, 'prueba2112', 'ctm', 'zczx', 'diego@gafghs.com', 'sad', 0),
(28, 'casi', 'ctm', 'asda', 'asd@gmadsf.cl', 'asdas', 0),
(29, 'armando', 'sitios', 'en laravel', 'aosjd@gskg.cl', 'asdasdkok', 1),
(30, 'sadf', 'adsf', 'adsf', 'adsf@ds.cl', 'asdasdkok', 1),
(31, 'dfg', 'dfgsdfg', 'adfgadfg', 'afdgafdg@dasijdi.cl', 'afg', 1),
(32, 'fdg', 'sdfg', 'sfdgs', 'sfdg@dsf.cl', 'asda', 1),
(33, 'hola', 'ewrwer', 'qwe', 'qwe@fsdf.cl', 'eqwe', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
