<?php
 
class UsuarioController extends BaseController {



	public function index()
	{	//me trae todos los usuarios y los imprime en index
		$usuarios = usuario::all();
		$id = usuario::all();
		$objcolaboradores = new usuario();
		$colaboradores = $objcolaboradores->obtener_datos();
		
		return View::make("index", array("colaboradores"=>$colaboradores));
		
	}

	public function login(){
		return View::make('login');
	}

	public function recibelogin(){
		/*$datos = Input::only(['email','pass']);
		$reglas = [
			'email' => 'required|email',
			'pass' => 'required',
		];
		$msg=[
			'required'=>'este campo es obligatorio',
			'email'=>'este campo es obligatorio'
		];

		$validacionLogin= Validator::make(Input::all(),$reglas,$msg);
		if($validacionLogin->passes()){
			*/if(Auth::attempt(
				['email'=>Input::get('email'),'password'=>Input::get('pass')]

				)){
					Auth::User()->nombre;
			
					return Redirect::to('index');
	
					}else{
						$datos = 'datos Incorrectos';
						return Redirect::to('/')->with('datos',$datos);
					}
		/*}else{
			dd('sale de aqui ctm!');
		}*/
	}
	public function logout(){
		Auth::logout();
		Session::flush();
		return Redirect::to('/');
	}


	

	
	public function crear(){
		
		return View::make('create');// me retorna el html que es llamado por el route
	}

	public function editar($idcolaborador){//parametro id mandado por la ruta 
		
		$objcolaborador = new usuario();
		$id_colaborador = $objcolaborador->obtener_id($idcolaborador);
		
		return View::make('editar', array('idcolaborador'=>$idcolaborador,'id_colaborador'=>$id_colaborador));
	}
	


	public function registrar(){

		$datos = Input::only(['nombres_post','apellido_post','rut_post','cargo_post','descripcion_post','email_post','activo_post']);
	
		$reglas = [
			'nombres_post' => 'required',
			'apellido_post'=>'required',
			'rut_post' => 'required',
			'cargo_post' => 'required',
			'descripcion_post' => ' required',
			'email_post' => 'required|email',
			'activo_post' => 'required'
		];
		$msg = ['required'=> 'este campo es obligatorio',
				'email' => 'Email no valido'
		];
		$validacion = Validator::make(Input::all(),$reglas,$msg);

		if($validacion->passes()) {
			
			$ingreso = usuario::guardar_usuario((object)$datos);
			if($ingreso==true){
				$se_ingreso = 'usuario registrado correctamente';
			}else{

				$se_ingreso = 'usuario no registrado';
			}
			
			return Redirect::to('index')->with('se_ingreso',$se_ingreso);
			
		}else{
			return Redirect::back()->withInput()->withErrors($validacion);
		}
		
		
	}

	public function actualizar($idcolaborador){
		
		$datos = Input::only(['nombres_post','apellido_post','cargo_post','descripcion_post','email_post','activo_post']);
		
		$reglas = [
			'nombres_post' => 'required',
			'apellido_post'=>'required',
			'cargo_post' => 'required',
			'descripcion_post' => ' required',
			'email_post' => 'required|email',
			'activo_post' => 'required'
		];
		$msg = ['required'=> 'este campo es obligatorio',
				'email' => 'Email no valido'
		];
		$validacion = Validator::make(Input::all(),$reglas,$msg);
	
		if($validacion->passes()) {
			$actualizar = usuario::actualizar_usuario($idcolaborador,(object)$datos);

			
			if($actualizar==true){
				$se_actualizo = 'datos actualizados correctamente ';
			}else{
				$se_actualizo ='No hubieron cambios ';
			}
			return Redirect::to('index')->with('se_actualizo',$se_actualizo);
		
		}
		else{
			return Redirect::back()->withInput()->withErrors($validacion);
			
		}
		
	
	}

	public function eliminar($idcolaborador){
		$desactivar_id = new usuario();
		$colaborador_desactivado = $desactivar_id->obtener_id_desactivado($idcolaborador);
	
		return View::make('desactivar',array('colaborador_desactivado'=> $colaborador_desactivado,'idcolaborador'=>$idcolaborador));


	}
	
	public function desactivarUsuario($idcolaborador){
		
		$datos = Input::only(['activo_desactivado']);
		
		$reglas = [
			
			'activo_desactivado' => 'required'
		];
		
		$msg = ['required'=> 'este campo es obligatorio'
		];
		$validacion = Validator::make(Input::all(),$reglas);
		
		if($validacion->passes()) {
		
			$actualizar = usuario::actualizar_estado($idcolaborador,(object)$datos);
			if($actualizar==true){

				$se_actualizo = 'datos actualizados correctamente ';
			}else{
				$se_actualizo ='No hubieron cambios ';
			}
			return Redirect::to('index')->with('actualizar',$actualizar);
		
			}
		else{
			
			return Redirect::back()->withInput()->withErrors($validacion);
			
		}
	}
	
}

