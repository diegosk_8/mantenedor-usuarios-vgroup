@extends('layout')
@section('contenido')

<div class="container">
	<div class="container-contact2">
		<div class="wrap-contact2">
		{{Form::open(['routes'=> 'registrar-usuario', 'method'=> 'POST', 'role'=> 'form'])}}
			<div class="form-group">
				<div class="wrap-input2 validate-input">
				{{form::label('nombres_post', 'nombre')}}
				{{form::text('nombres_post', null, ['class'=>'input2'])}}
				@if ($errors->first('nombres_post'))
				{{$errors->first('nombres_post')}}
				@endif
				</div>
				<div class="wrap-input2 validate-input">
					{{form::label('apellido_post', 'apellido')}}
					{{form::text('apellido_post', null, ['class'=>'input2'])}}
					@if ($errors->first('apellido_post'))
					URL Foto: {{$errors->first('apellido_post')}}
					@endif
					</div>
					<div class="wrap-input2 validate-input">
						{{form::label('rut_post', 'rut')}}
						{{form::text('rut_post', null, ['class'=>'input2'])}}
						@if ($errors->first('rut_post'))
						URL Foto: {{$errors->first('rut_post')}}
						@endif
						</div>
				<div class="wrap-input2 validate-input">
				{{form::label('cargo_post', 'cargo')}}
				{{form::text('cargo_post', null, ['class'=>'input2'])}}
				@if ($errors->first('cargo_post'))
				cargo: {{$errors->first('cargo_post')}}
				@endif
				</div>
				<div class="wrap-input2 validate-input">
				{{form::label('descripcion_post', 'descripcion')}}
				{{form::textarea('descripcion_post', null, ['class'=>'input2'])}}
				@if ($errors->first('descripcion_post'))
				descripcion: {{$errors->first('descripcion_post')}}
				@endif
				</div>
				<div class="wrap-input2 validate-input">
				{{form::label('email_post', 'email')}}
				{{form::email('email_post', null, ['class'=>'input2'])}}
				@if ($errors->first('email_post'))
				email: {{$errors->first('email_post')}}
				@endif
				</div>
				
				<div class="wrap-input2 validate-input">
				{{form::label('activo_post', 'activo')}}
				{{Form::select('activo_post',array(1 => 'Activo', 0 => 'Inactivo'),1,['class'=>'input2'])}}
				@if ($errors->first('activo_post'))
				estado: {{$errors->first('activo_post')}}
				@endif
				</div>
			</div>
			<button type="submit" value="registrar" class="btn btn-success btn_guardar">Guardar</button>
		{{Form::close()}}

		</div>	
</div>
</div>





@stop