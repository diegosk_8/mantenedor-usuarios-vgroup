@extends('layout')
@section('contenido-eliminar')

<button  class="btn btn-danger text-white" data-toggle="modal" data-target="#exampleModalCenter">Eliminar</button>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{Form::open(['routes'=> 'desactivar/'.$id, 'method'=> 'POST', 'role'=> 'form'])}}
      <div class="modal-body">
      <p>estas seguro de querer eliminar? </p>
      
      
      {{Form::hidden('estado_desactivado','0',['class'=>'form-control'])}}
      
      </div>
      <div class="modal-footer">
      <a type="button" class="btn btn-secondary" data-dismiss="modal" href="{{URL::to('/')}}">No</a>
        <button type="submit" class="btn btn-primary">Si</button>
      </div>
      {{Form::close()}}

    </div>
  </div>






 



        
@endsection
@stop