@extends('layout')
@section('contenido')
<script type="text/javaScript"> 
      
        $(document).ready(function(){
          $('#modaldesactivar').on('shown.bs.modal', function (event) {
          var id = $(event.relatedTarget).data("id");
          $('#modaldesactivar form').attr('action','{{URL::to("desactivar-usuario")}}'+'/'+id);
        })
        })

        $(document).ready(function(){
          $('#modal_habilitar').on('shown.bs.modal', function (event) {
          var idH = $(event.relatedTarget).data("id");
          $('#modal_habilitar form').attr('action','{{URL::to("desactivar-usuario")}}'+'/'+idH);
        })
        })

</script>
<!-- mensajes -->
<style>
.usuario:hover{
  background: rgba(0, 0, 0, 0.1)
}
#btn-editar:hover{
  
}



</style>


<!-- fin mensajes -->

<div id="contenedor" class="container  " style="background: rgba(240, 235, 236, 0.8); margin-left:29px;" >
    <div class="container">
        @if (Session::has('se_ingreso'))
        <div class="alert alert-primary" role="alert">
                {{Session::get('se_ingreso')}}
              </div>
        
        @endif

        @if (Session::has('se_actualizo'))
        <div class="alert alert-primary" role="alert">
        {{Session::get('se_actualizo')}}
        </div>

@endif
</div>

    <div class="row">
            @if(count($colaboradores) > 0 )
              @foreach($colaboradores as $colaborador)
                <li class="mt-5">    
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6"  id="usuario" style="display: nome;">
                        @if ($colaborador->activo == '1')
                            <div class="usuario   card pr-2 pl-2 ml-3" style="width:230px;">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="{{asset('images/habilitado.png')}}" alt="card image"></p>
                        @elseif ($colaborador->activo == '0')
                            <div class="usuario  card pr-2 pl-2  ml-3" style="width:230px; ">
                                <div id="desactivado" class="card-body text-center">
                                    <p><img class=" img-fluid" src="{{asset('images/desabilitado.png')}}" alt="card image"></p>
                        @endif
                        
                                <h4 class="card-title">{{ $colaborador->nombres}} {{$colaborador->apellido}}</h4>
                            <p class="card-text">{{$colaborador->descripcion}}</p> 
                            <p class="card-text">{{ $colaborador->cargo }}</p>
                            <p class="card-text">{{$colaborador->email}}</p>
                            <p class="card-text" id="estado" data-estado="{{$colaborador->activo}}">
                                @if ($colaborador->activo == '1')    
                                    <p class="card-text">activo</p> 
                                    <a class="btn " href="{{URL::to('editar-usuario/'.$colaborador->id_colaborador)}}"><img id="btn-editar" style="width:30px; heigth:30px;" src="{{asset('images/editar.png')}}" ></a>
                            <a  class="btn  text-white" data-toggle="modal" data-target="#modaldesactivar" data-id="{{$colaborador->id_colaborador}}"><img id="btn-eliminar" style="width:30px; heigth:30px;" src="{{asset('images/eliminar.png')}}" alt=""></a>
                                @elseif($colaborador->activo == '0')
                                    <p class="card-text">Desabilitado</p>
                            <a class="btn btn-primary text-white" data-toggle="modal" data-id="{{$colaborador->id_colaborador}}" data-target="#modal_habilitar">HABILITAR</a>
                                @endif
                            </p>
                        </div>
                            </div>
                        </div>
                            
                </li>
              @endforeach
            @endif  
    </div>

</div>




<!-- Modal desactivar-->
<div class="modal fade" id="modaldesactivar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{Form::open(['url'=> '#', 'method'=> 'POST', 'role'=> 'form'])}}
        <div class="modal-body">
        <p>estas seguro de querer Desabilitar? </p>
        {{Form::hidden('activo_desactivado','0',['class'=>'form-control'])}}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" ">No</button>
          <button type="submit" class="btn btn-primary">Si</button>
        </div>
        {{Form::close()}}
      </div>
    </div>
  </div>
<!-- fin modal eliminar-->
<!-- Modal Habilitar-->
<div class="modal" id="modal_habilitar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                {{Form::open(['url'=> '#', 'method'=> 'POST', 'role'=> 'form'])}}
                <div class="modal-body">
                <p>estas seguro de querer habilitar ?</p>
                {{Form::hidden('activo_desactivado','1',['class'=>'form-control'])}}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  <button type="submit" class="btn btn-primary">Si</button>
                </div>
                {{Form::close()}}
      </div>
    </div>
  </div>
<!--fin modal habilitar-->
@stop