@extends('layout')
@section('form-editar')

<div class="container" >
		<div class="container-contact2">
				<div class="wrap-contact2">
	{{Form::open(['url'=> 'actualizar-usuario/'.$idcolaborador, 'method'=> 'POST', 'role'=> 'form'])}}

		<div class="form-group">
			<div class="wrap-input2 validate-input">
			{{form::label('nombres_post', 'nombres')}}
			{{form::text('nombres_post', $id_colaborador->nombres, ['class'=>'form-control'])}}
			@if ($errors->first('nombres_post'))
			{{$errors->first('nombres_post')}}
			@endif
			</div>
			<div class="wrap-input2 validate-input">
				{{form::label('apellido_post', 'apellido')}}
				{{form::text('apellido_post', $id_colaborador->apellido, ['class'=>'form-control'])}}
				@if ($errors->first('apellido_post'))
				{{$errors->first('apellido_post')}}
				@endif
				</div>
			<div class="wrap-input2 validate-input">
			{{form::label('cargo_post', 'cargo')}}
			{{form::text('cargo_post', $id_colaborador->cargo, ['class'=>'form-control'])}}
			@if ($errors->first('cargo_post'))
			cargo: {{$errors->first('cargo_post')}}
			@endif
			</div>
			<div class="wrap-input2 validate-input">
			{{form::label('descripcion_post', 'descripcion')}}
			{{form::textarea('descripcion_post', $id_colaborador->descripcion, ['class'=>'form-control'])}}
			@if ($errors->first('descripcion_post'))
			descripcion: {{$errors->first('descripcion_post')}}
			@endif
			</div>
			<div class="wrap-input2 validate-input">
			{{form::label('email_post', 'email')}}
			{{form::email('email_post', $id_colaborador->email, ['class'=>'form-control'])}}
			@if ($errors->first('email_post'))
			email: {{$errors->first('email_post')}}
			@endif
			</div>
			
			<div class="wrap-input2 validate-input">
				{{form::label('activo_post', 'activo')}}
				<select name="activo_post" id="" class="form-control">
					<?php
					switch ($id_colaborador->activo) {
						case '1':
						echo ' <option selected="selected" value="1" >Activo</option>';
						echo '<option value="0">Inactivo</option>';
							break;
						case '0':
						echo ' <option value="1" >Activo</option>';
						echo '<option selected="selected" value="0">Inactivo</option>';
							break;
						default:
							# code...
							break;
					}	
					?>
					
				</select>
			
			
			
			@if ($errors->first('activo_post'))
			estado: {{$errors->first('activo_post')}}
			@endif
			</div>
		</div>
		
		<button type="submit" value="actualizar" class="btn btn-success btn_guardar">Guardar Cambios</button>
	{{Form::close()}}

				</div>
		</div>
</div>
</div>
@endsection
@stop