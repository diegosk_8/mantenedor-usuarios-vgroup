<?php
class usuario extends Eloquent{

    protected $fillable = ['nombres','apellido', 'cargo', 'descripcion', 'email', 'activo'];
    //metodo para obtener los datos guardados en la base de datos 
    public function obtener_datos(){
        // DB::table (nombre de la tabla ) -> get( que me trae todos los registros que estan el la tabla usuarios)
        $consulta = DB::table ('tblcolaboradores')->get();
        return $consulta;
    }

    public function obtener_estado(){

        $estado = DB::table ('tblcolaboradores')->last();
        return $estado;

    }
    //metodo para guardar los datos ingresados en el formularios 
    public static function guardar_usuario($datos){ //recibe un parametro 'datos' que esta en el controlador 
        $insert = DB::table('tblcolaboradores') // DB::table(nombre de la tabla) -> insert(['nombre del campo de la base de datos' => va hacer igual al parametrp 'datos' y el campo del formulario]) 
        ->insert([
            'nombres'=>$datos->nombres_post,
            'apellido'=>$datos->apellido_post,
            'rut'=>$datos->rut_post,
            'cargo'=>$datos->cargo_post,
            'descripcion'=>$datos->descripcion_post,
            'email'=>$datos->email_post,
            'activo'=>$datos->activo_post
        ]);

       
        return $insert;
    }

    

    public static function obtener_id($idcolaborador){

        $usuario_obtenido_porID = DB::table('tblcolaboradores')->where('id_colaborador',$idcolaborador)->first();
       // dd($usuario_obtenido_porID);
        return $usuario_obtenido_porID;
    }
    public static function obtener_id_desactivado($id){

        $id_desactivado = DB::table('tblcolaboradores')->where('id_colaborador',$id)->first();
       // dd($usuario_obtenido_porID);
        return $id_desactivado;
    }

    public static function actualizar_usuario($idcolaborador,$form){
       
        $update = DB::table('tblcolaboradores')->where('id_colaborador',$idcolaborador)

        ->update(
            [
                'nombres'=>$form->nombres_post,
                'apellido'=>$form->apellido_post,
                'cargo'=>$form->cargo_post,
                'descripcion'=>$form->descripcion_post,
                'email'=>$form->email_post,
                'activo'=>$form->activo_post
            ]
        );
        
        return $update;
    }

    public static function actualizar_estado($idcolaborador,$form){
        $update = DB::table('tblcolaboradores')->where('id_colaborador', $idcolaborador)
        ->update([
                'activo'=>$form->activo_desactivado
        ]);
        return $update;
    }

    
}