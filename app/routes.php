<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', ['as'=>'login','uses'=> 'UsuarioController@login' ]);
Route::post('log', ['as'=>'login','uses'=> 'UsuarioController@recibelogin' ]);

Route::group(['before' => 'auth'], function(){

Route::get('salir', 'UsuarioController@logout');
Route::get('index', 'UsuarioController@index');//ruta que me lleba al index o ruta raiz 
Route::get('crear-usuario', 'UsuarioController@crear'); //
Route::post('crear-usuario', ['as' =>'registrar','uses'=>'UsuarioController@registrar']);


Route::get('editar-usuario/{idcolaborador}', ['as' =>'actualizar','uses'=>'UsuarioController@editar']);
Route::post('actualizar-usuario/{idcolaborador}', ['as' =>'actualizado','uses'=>'UsuarioController@actualizar']);

route::get('desactivar-usuario',['as'=>'desactivar','uses'=>'UsuarioController@eliminar']);
route::get('desactivar-usuario/{idcolaborador}',['as'=>'desactivarid','uses'=>'UsuarioController@eliminar']);
route::post('desactivar-usuario/{idcolaborador}',['as'=>'desactivar-usuario','uses'=> 'UsuarioController@desactivarusuario']);

});